 //
//  AppDelegate.m
//  Collext
//
//  Created by Nitin Kumar on 16/03/16.
//  Copyright © 2016 ChromeInfo Technologies. All rights reserved.
//

#import "AppDelegate.h"
#import "CLCustomNavigationController.h"
#import "CLLoginViewController.h"
#import "CLLandingViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    sleep(2);
    
    // Check for internet connection is available or not.
    [[CLWebRequestHandler sharedInstance] monitorInternetConnection];
    
    if (/* DISABLES CODE */ (1))
    {
        [self gotoLoginView];
    }
    else
    {
        [self gotoHomeView];
    }
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark - User Defined Methods -

- (void)gotoLoginView
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    CLLandingViewController *landingViewController = [mainStoryboard instantiateViewControllerWithIdentifier:NSStringFromClass([CLLandingViewController class])];
    
    CLCustomNavigationController *customNavController = [[CLCustomNavigationController alloc] initWithRootViewController:landingViewController];
    
    self.window.rootViewController = customNavController;
}

- (void)gotoHomeView
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    CLLoginViewController *loginViewController = [mainStoryboard instantiateViewControllerWithIdentifier:NSStringFromClass([CLLoginViewController class])];
    
    CLCustomNavigationController *customNavController = [[CLCustomNavigationController alloc] initWithRootViewController:loginViewController];
    
    self.window.rootViewController = customNavController;
}

@end
