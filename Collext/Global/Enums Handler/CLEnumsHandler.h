//
//  EnumsHandler.h
//
//
//  Created by Nitin Kumar on 20/05/15.
//  Copyright (c) 2015 Nitin Kumar. All rights reserved.
//

#ifndef Collext_EnumsHandler_h
#define Collext_EnumsHandler_h

#endif


// This enum will specify the custom toolbar button tags
typedef NS_ENUM(NSInteger, UIToolbarButtonTags)
{
    UIToolbarButtonTagPrevious = 1,
    UIToolbarButtonTagNext     = 2,
    UIToolbarButtonTagDone     = 3
};

