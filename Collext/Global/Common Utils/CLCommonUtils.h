//
//  IPCommonUtils.h
//  InsightPortal
//
//  Created by Nitin Kumar on 26/11/15.
//  Copyright (c) 2015 ChromeInfo Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


/*!
 * This class is contains all the common utilities with in the entire App such as common methods.
 */
@interface CLCommonUtils : NSObject


/*!
 @description To show alert view with message.
 @return Returns None.
 */
+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
               buttonTitle:(NSString *)buttonTitle
      andCompletionHandler:(void(^)(BOOL status))completionBlock;

@end
