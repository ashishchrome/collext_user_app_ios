//
//  IPCommonUtils.m
//  InsightPortal
//
//  Created by Nitin Kumar on 26/11/15.
//  Copyright (c) 2015 ChromeInfo Technologies. All rights reserved.
//

#import "CLCommonUtils.h"

@implementation CLCommonUtils

+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
               buttonTitle:(NSString *)buttonTitle
      andCompletionHandler:(void(^)(BOOL status))completionBlock

{
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                                 message:message
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        
    UIAlertAction *alertActionOK = [UIAlertAction actionWithTitle:buttonTitle ? buttonTitle : kOkTitle
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * _Nonnull action) {
                                                                  
                                                                  if (completionBlock)
                                                                  {
                                                                      completionBlock(YES);
                                                                  }
                                                                  
                                                              }];
        
    [alertController addAction:alertActionOK];
        
    [[UIApplication sharedApplication].delegate.window.rootViewController presentViewController:alertController
                                                                                           animated:YES
                                                                                         completion:nil];
}


@end
