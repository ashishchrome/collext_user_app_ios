//
//  CLMacroHandler.h
//  Collext
//
//  Created by Nitin Kumar on 17/03/16.
//  Copyright © 2016 ChromeInfo Technologies. All rights reserved.
//

#ifndef CLMacroHandler_h
#define CLMacroHandler_h


// Common Macros
#define METHOD_START                    if(NO) NSLog(@"Method Start: %s",__FUNCTION__);
#define METHOD_END                      if(NO) NSLog(@"Method End: %s",__FUNCTION__);
#define APP_DELEGATE                    ((AppDelegate*) ([UIApplication sharedApplication].delegate))
#define SCREEN_SIZE                     [[UIScreen mainScreen] bounds]
#define DEVICE_UID_STRING               [[[UIDevice currentDevice] identifierForVendor] UUIDString]
#define IS_IPHONE_5         ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE_4         ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )480 ) < DBL_EPSILON )
#define DARK_ORANGE_COLOR     [UIColor colorWithRed:255.0f/255.0f green:72.0f/255.0f blue:0.0f/255.0f alpha:255.0f/255.0f]


#define BORDER_WIDTH 2.0
#define BORDER_COLOR [[UIColor whiteColor] CGColor]
#define CORNER_RADIUS 7.0


#endif /* CLMacroHandler_h */
