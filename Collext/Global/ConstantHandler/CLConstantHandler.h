//
//  IPConstantStringHandler.h
//  InsightPortal
//
//  Created by Nitin Kumar on 23/10/15.
//  Copyright (c) 2015 ChromeInfo Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CLConstantHandler : NSObject

#pragma mark - Key Constant -

extern NSString *const kPasswordKey;
extern NSString *const kErrorKey;
extern NSString *const kErrorDetailsKey;
extern NSString *const kDetailKey;
extern NSString *const kEmailKey;
extern NSString *const kIdKey;
extern NSString *const kLastResetTimeKey;
extern NSString *const kLastvisitDateKey;
extern NSString *const kNameKey;
extern NSString *const kUsernameKey;
extern NSString *const kTitleKey;
extern NSString *const kIPUserKey;
extern NSString *const kRegisterDateKey;
extern NSString *const kTotalNewsKey;
extern NSString *const kCategoryNewsKey;
extern NSString *const kUserTokenKey;
extern NSString *const kDeviceTypeKey;
extern NSString *const kDeviceIDKey;
extern NSString *const kSessionTokenKey;
extern NSString *const kRegisterIdKey;
extern NSString *const kIsNotifyKey;
extern NSString *const kPageNumberKey;
extern NSString *const kUserIdKey;
extern NSString *const kContentIdKey;
extern NSString *const kAppVersionKey;



#pragma mark - URL Constant -

extern NSString *const kHostURL;
extern NSString *const kLoginURL;
extern NSString *const kDashboardURL;
extern NSString *const kImageHostURL;
extern NSString *const kMonthWiseEventURL;
extern NSString *const kUpdateProfileURL;
extern NSString *const kUserLogoutURL;
extern NSString *const kSupportURL;
extern NSString *const kTotalEventsURL;
extern NSString *const kAllSubCateWithDataURL;
extern NSString *const kSubCategoryDataURL;
extern NSString *const kSimpleSearchURL;
extern NSString *const kAdvanceSearchURL;
extern NSString *const kArticleContentURL;
extern NSString *const kForgotPasswordURL;
extern NSString *const kNewsSinceLastLoginURL;
extern NSString *const kNewsAlertsURL;
extern NSString *const kNewsFromLastMonthURL;
extern NSString *const kNewsCategoryURL;
extern NSString *const kNewsSoftwareURL;
extern NSString *const kNewsRumoursURL;
extern NSString *const kShareArticleURL;
extern NSString *const kPrivacyPolicyURL;
extern NSString *const kGetProfileURL;
extern NSString *const qtfLogoURLString;



#pragma mark - Popup Messages -

extern NSString *const kCheckInternetMsg;
extern NSString *const kDefaultErrorMsg;
extern NSString *const kNoInternetText;
extern NSString *const kPasswordInvalidMessage;
extern NSString *const kUsernameInvalidMessage;
extern NSString *const kInvalidDateSelectedMsg;
extern NSString *const kNoSearchTextMsg;
extern NSString *const kNoArticleFoundMsg;
extern NSString *const kSessionExpiredMsg;
extern NSString *const kEmptyNameMsg;
extern NSString *const kNameLimitMsg;
extern NSString *const kEmptyEmailMsg;
extern NSString *const kEmailLimitMsg;
extern NSString *const kInvalidEmailMsg;
extern NSString *const kEmailNotMatchMsg;
extern NSString *const kPasswordLimitMsg;
extern NSString *const kPasswordNotMatchMsg;



#pragma mark - Image Names -

extern NSString *const kToolbarLeftOrangeArrowImage;
extern NSString *const kToolbarRightOrangeArrowImage;
extern NSString *const kToolbarLeftGreyArrowImage;
extern NSString *const kToolbarRightGreyArrowImage;


#pragma mark - Notification -

extern NSString *const kAppInForegroundNotification;
extern NSString *const kpushNotification;


#pragma mark - Other -

extern NSString *const kTryAgainTitle;
extern NSString *const kWarningTitle;
extern NSString *const kDashboardTitle;
extern NSString *const kDesktopTitle;
extern NSString *const kMobileTitle;
extern NSString *const kSoftwareTitle;
extern NSString *const kEventsTitle;
extern NSString *const kRumourTrendsTitle;
extern NSString *const kSupportTitle;
extern NSString *const kProfileTitle;
extern NSString *const kServerDateFormat;
extern NSString *const kDayMonthYearFormat;
extern NSString *const kShareArticleKey;
extern NSString *const kSupportEmailString;
extern NSString *const kOpenSansFont;
extern NSString *const kYearMonthDayFormat;
extern NSString *const kStringEmpty;
extern NSString *const kStringZero;
extern NSString *const kStringOne;
extern NSString *const kAdvanceSearchTitle;
extern NSString *const kNewsListTitle;
extern NSString *const kArticleTitle;
extern NSString *const kArticleCellId;
extern NSString *const kOkTitle;
extern NSString *const kIsNotificationPush;


@end




