//
//  IPConstantStringHandler.m
//  InsightPortal
//
//  Created by Nitin Kumar on 23/10/15.
//  Copyright (c) 2015 ChromeInfo Technologies. All rights reserved.
//

#import "CLConstantHandler.h"

@implementation CLConstantHandler


#pragma mark - Key Constant -

NSString *const kPasswordKey        = @"password";
NSString *const kErrorKey           = @"error";
NSString *const kErrorDetailsKey    = @"errordetails";
NSString *const kDetailKey          = @"detail";
NSString *const kEmailKey           = @"email";
NSString *const kIdKey              = @"id";
NSString *const kLastResetTimeKey   = @"lastResetTime";
NSString *const kLastvisitDateKey   = @"lastvisitDate" ;
NSString *const kNameKey            = @"name";
NSString *const kUsernameKey        = @"username";
NSString *const kTitleKey           = @"title";
NSString *const kIPUserKey          = @"IPUser";
NSString *const kRegisterDateKey    = @"registerDate";
NSString *const kTotalNewsKey       = @"total_news";
NSString *const kCategoryNewsKey    = @"category_news";
NSString *const kUserTokenKey       = @"user_token";
NSString *const kDeviceTypeKey      = @"device_type";
NSString *const kDeviceIDKey        = @"device_id";
NSString *const kSessionTokenKey    = @"session_token";
NSString *const kRegisterIdKey      = @"register_id";
NSString *const kIsNotifyKey        = @"is_notify";
NSString *const kPageNumberKey      = @"page_number";
NSString *const kUserIdKey          = @"user_id";
NSString *const kContentIdKey       = @"content_id";
NSString *const kAppVersionKey      = @"appVersion";



#pragma mark - URL Constant -

NSString *const kHostURL                = @"http://collext.com/index.php/mobile/";
NSString *const kLoginURL               = @"User_Login/";

NSString *const kDashboardURL           = @"/Dashboard_Api";
NSString *const kMonthWiseEventURL      = @"/Dashboard_Api/month_wise_event";
NSString *const kUpdateProfileURL       = @"/Update_Profile_Api";
NSString *const kUserLogoutURL          = @"/Login_Api/logout_user";
NSString *const kSupportURL             = @"/Support_Api/send_email";
NSString *const kTotalEventsURL         = @"/Dashboard_Api/total_event_list";
NSString *const kAllSubCateWithDataURL  = @"/Dashboard_Api/subcategories";
NSString *const kSubCategoryDataURL     = @"/Content_Api/subcategory_data";
NSString *const kSimpleSearchURL        = @"/Support_Api/list_of_search";
NSString *const kArticleContentURL      = @"/Content_Api/article";
NSString *const kAdvanceSearchURL       = @"/Support_Api/advance_search";
NSString *const kNewsSinceLastLoginURL  = @"/Content_Api/dashboard_news";
NSString *const kNewsAlertsURL          = @"/Content_Api/alert_news_list";
NSString *const kNewsFromLastMonthURL   = @"/Content_Api/last_news";
NSString *const kNewsCategoryURL        = @"/Content_Api/last_login_news";
NSString *const kNewsSoftwareURL        = @"/Content_Api/last_login_news_software";
NSString *const kNewsRumoursURL         = @"/Content_Api/rumour_news_list";
NSString *const kShareArticleURL        = @"/Share_Article_Api";
NSString *const kForgotPasswordURL      = @"https://insight.qualitytaskforce.com/index.php/guest-login?view=reset";
NSString *const kPrivacyPolicyURL       = @"http://qualitytaskforce.com/privacy-policy-insightportal-app";
NSString *const kGetProfileURL          = @"/Update_Profile_Api/get_profile";
NSString *const qtfLogoURLString        = @"https://qa.insight.qualitytaskforce.com/images/Logos/QTF.jpg";



#pragma mark - Popup Messages -

NSString *const kCheckInternetMsg       = @"Please re-check your internet connection and try again.";
NSString *const kDefaultErrorMsg        = @"Oops Something Has Gone Wrong!";
NSString *const kNoInternetText         = @"No Internet Connection";
NSString *const kPasswordInvalidMessage = @"Password should not be empty.";
NSString *const kUsernameInvalidMessage = @"Username should not be empty.";
NSString *const kInvalidDateSelectedMsg = @"Please enter a valid date, 'from date' is beyond the 'to date'.";
NSString *const kNoSearchTextMsg        = @"Please enter a valid search text.";
NSString *const kNoArticleFoundMsg      = @"Unfortunately there are no new articles!";
NSString *const kSessionExpiredMsg      = @"Your session has been expired. Please login again.";
NSString *const kEmptyNameMsg           = @"Name should not be empty.";
NSString *const kNameLimitMsg           = @"Name should have at least 4 characters.";
NSString *const kEmptyEmailMsg          = @"Email address should not be empty.";
NSString *const kEmailLimitMsg          = @"Email address should have at least 4 characters.";
NSString *const kInvalidEmailMsg        = @"Email address should be valid.";
NSString *const kEmailNotMatchMsg       = @"The email addresses you entered do not match.";
NSString *const kPasswordLimitMsg       = @"Password should have at least 4 characters.";
NSString *const kPasswordNotMatchMsg    = @"The passwords you have entered do not match.";



#pragma mark - Image Names -

NSString *const kToolbarLeftOrangeArrowImage    = @"left_arrow_orange.png";
NSString *const kToolbarRightOrangeArrowImage   = @"right_arrow_orange.png";
NSString *const kToolbarLeftGreyArrowImage      = @"left_arrow_gray.png";
NSString *const kToolbarRightGreyArrowImage     = @"right_arrow_gray.png";



#pragma mark - Notification -

NSString *const kAppInForegroundNotification    = @"appInForeground";
NSString *const kpushNotification               = @"pushNotification";



#pragma mark - Other -

NSString *const kTryAgainTitle      = @"Try Again";
NSString *const kWarningTitle       = @"Warning";
NSString *const kDashboardTitle     = @"Dashboard";
NSString *const kDesktopTitle       = @"Desktop";
NSString *const kMobileTitle        = @"Mobile";
NSString *const kSoftwareTitle      = @"Software";
NSString *const kEventsTitle        = @"Events";
NSString *const kRumourTrendsTitle  = @"Rumours and Trends";
NSString *const kSupportTitle       = @"Support";
NSString *const kAdvanceSearchTitle = @"Advanced Search";
NSString *const kNewsListTitle      = @"News List";
NSString *const kProfileTitle       = @"Profile";
NSString *const kServerDateFormat   = @"yyyy-MM-dd HH:mm:ss";
NSString *const kDayMonthYearFormat = @"dd MMM yyyy";
NSString *const kShareArticleKey    = @"shareArticleNotification";
NSString *const kSupportEmailString = @"insight@qualitytaskforce.com";
NSString *const kOpenSansFont       = @"OpenSans";
NSString *const kYearMonthDayFormat = @"yyyy-MM-dd";
NSString *const kStringEmpty        = @"";
NSString *const kStringZero         = @"0";
NSString *const kStringOne          = @"1";
NSString *const kArticleTitle       = @"Article";
NSString *const kArticleCellId      = @"articleCell";
NSString *const kOkTitle            = @"OK";
NSString *const kIsNotificationPush = @"isPushNotificationPushed";







@end