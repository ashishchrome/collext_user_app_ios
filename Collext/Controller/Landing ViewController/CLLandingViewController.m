//
//  LandingViewController.m
//  Collext
//
//  Created by Nitin Kumar on 16/03/16.
//  Copyright © 2016 ChromeInfo Technologies. All rights reserved.
//

#import "CLLandingViewController.h"
#import "CLLoginViewController.h"
#import "CLSignUpViewController.h"

@interface CLLandingViewController ()

@end

@implementation CLLandingViewController

#pragma mark - LifeCycle -

- (void)viewDidLoad
{
    [super viewDidLoad];
}


#pragma mark - IBActions -

- (IBAction)loginButtonTapped:(UIButton *)sender
{
    // Go to login screen.
    CLLoginViewController *loginViewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([CLLoginViewController class])];
    
    [self.navigationController pushViewController:loginViewController animated:YES];
}

- (IBAction)signUpButtonTapped:(UIButton *)sender
{
    // Go to sign up screen.
    CLSignUpViewController *signupViewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([CLSignUpViewController class])];
    
    [self.navigationController pushViewController:signupViewController animated:YES];
}



#pragma mark - Memory Methods -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
