//
//  CLLoginViewController.m
//  Collext
//
//  Created by Nitin Kumar on 16/03/16.
//  Copyright © 2016 ChromeInfo Technologies. All rights reserved.
//

#import "CLLoginViewController.h"
#import "CLSignUpViewController.h"

@interface CLLoginViewController () <UICustomToolbarDelegate>
{
    NSInteger _activeInputTag;
}

// IBOutlets
@property (weak, nonatomic) IBOutlet UIView             *containerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewWidthConstraint;
@property (weak, nonatomic) IBOutlet UITextField        *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField        *passwordTextField;

// User Defined Properties
@property (strong, nonatomic) CLCustomToolBar           *customToolbar;

@end

@implementation CLLoginViewController

#pragma mark - LifeCycle -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // To setup some initial UI components
    [self setupInitialView];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    // Update container contraint according to content.
    self.containerViewHeightConstraint.constant = 600;
    self.containerViewWidthConstraint.constant  = self.view.frame.size.width;
    [self.containerView updateConstraints];
    
    [self.view layoutSubviews];
}


#pragma mark - IBActions -

- (IBAction)forgotPasswordTapped:(UIButton *)sender
{
    NSLog(@"%s",__FUNCTION__);
}

- (IBAction)loginButtonTapped:(UIButton *)sender
{
    [self performLogin];
}

- (IBAction)signUpButtonTapped:(UIButton *)sender
{
    // Go to sign up screen.
    CLSignUpViewController *signupViewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([CLSignUpViewController class])];
    
    [self.navigationController pushViewController:signupViewController animated:YES];
}


#pragma mark - User Defined Methods -

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.emailTextField.text = @"vikasp@chr1omeinfotech";
    self.passwordTextField.text = @"9990291617";
    
}

- (CLCustomToolBar *)customToolbar
{
    if (_customToolbar == nil)
    {
        // Create toolbar object with control options.
        _customToolbar = [[CLCustomToolBar alloc] initWithToolbarPrevButtonImageName:kToolbarLeftOrangeArrowImage
                                                           nextButtonImageName:kToolbarRightOrangeArrowImage
                                                               doneButtonTitle:@"Done"
                                                                  withDelegate:self];
    }
    
    return _customToolbar;
}

- (void)performLogin
{
    [self.view endEditing:YES];
    
    // Check empty fields
    if ([self.emailTextField.text length])
    {
        if ([self.passwordTextField.text length])
        {
            [self postLoginAPIRequest];
        }
        else
        {
            [CLCommonUtils showAlertWithTitle:kWarningTitle
                                      message:kPasswordInvalidMessage
                                  buttonTitle:kOkTitle
                         andCompletionHandler:nil];
        }
    }
    else
    {
        [CLCommonUtils showAlertWithTitle:kWarningTitle
                                  message:kEmptyEmailMsg
                              buttonTitle:kOkTitle
                     andCompletionHandler:nil];
    }
}

- (void)postLoginAPIRequest
{
    if ([CLWebRequestHandler sharedInstance].isOnline)
    {
        [self showHUDView:YES];
        
        NSMutableString *completePath   = [[NSMutableString alloc] initWithString:kHostURL];
        [completePath appendString:kLoginURL];
        
        NSDictionary *params            = [[NSDictionary alloc] initWithObjectsAndKeys:
                                           self.emailTextField.text, kEmailKey,
                                           self.passwordTextField.text, kPasswordKey,
                                           nil];
        
        
        [[CLWebRequestHandler sharedInstance]
         sendPostRequestWithUrl:completePath
         parameters:params
         andCompletionHandler:^(NSDictionary *response)
         {
             [self showHUDView:NO];
             
             // If some error has been occured.
             if (![[response objectForKey:kErrorKey] isEqual:@0])
             {
                 NSLog(@"%s",__FUNCTION__);
             }
             else
             {
                 NSLog(@"%s",__FUNCTION__);
             }
         }
         andOnFailure:^(bool isFailed)
         {
             [self showHUDView:NO];
             
             [CLCommonUtils showAlertWithTitle:kWarningTitle
                                       message:kDefaultErrorMsg
                                   buttonTitle:kOkTitle
                          andCompletionHandler:nil];
         }];
    }
    else
    {
        [CLCommonUtils showAlertWithTitle:kWarningTitle
                                  message:kCheckInternetMsg
                              buttonTitle:kOkTitle
                     andCompletionHandler:nil];
    }
}

- (void)showHUDView:(BOOL)show
{
    if (show)
    {
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        
        [SVProgressHUD show];
    }
    else
    {
        [SVProgressHUD dismiss];
    }
}


#pragma mark - TextField Delegates -

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    _activeInputTag = textField.tag;
    
    if (textField == self.emailTextField)
    {
        [self.customToolbar disblePreviousButton:YES withImageName:kToolbarLeftGreyArrowImage];
        [self.customToolbar disbleNextButton:NO withImageName:kStringEmpty];
    }
    else
    {
        [self.customToolbar disbleNextButton:YES withImageName:kToolbarRightGreyArrowImage];
        [self.customToolbar disblePreviousButton:NO withImageName:kStringEmpty];
    }
    
    textField.inputAccessoryView = self.customToolbar;
    
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.emailTextField)
    {
        [_passwordTextField becomeFirstResponder];
    }
    else
    {
        [self performLogin];
    }
    
    return NO;
}


#pragma mark - Custom Toolbar Delegate -

- (void)didToolbarButtonSelectedWithTag:(NSInteger)tag
{
    METHOD_START
    
    switch (tag)
    {
        case UIToolbarButtonTagPrevious:
        {
            [(UITextField *)[self.view viewWithTag:_activeInputTag-1] becomeFirstResponder];
        }
            
            break;
            
        case UIToolbarButtonTagNext:
        {
            [(UITextField *)[self.view viewWithTag:_activeInputTag+1] becomeFirstResponder];
        }
            
            break;
            
        case UIToolbarButtonTagDone:
            
            // Perform on done button clicked. To set editing as end.
            [self.view endEditing:YES];
            break;
            
        default:
            break;
    }
}



#pragma mark - Memory Methods -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
