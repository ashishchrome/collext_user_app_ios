//
//  CustomToolBar.h
//  WhoHasTheKids
//
//  Created by Nitin Kumar on 13/05/15.
//  Copyright (c) 2015 Nitin Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UICustomToolbarDelegate <NSObject>

@optional

/*!
 @description:This Method will call on toolbar button selected.
 @param:tag This parameter will store the selected toolbar button's tag number
 */
- (void)didToolbarButtonSelectedWithTag:(NSInteger)tag;

@end


@interface CLCustomToolBar : UIToolbar

/*!
 @description This method is used for initialize toolbar instance variables with delegate. It is used when need to show the title on control button instead of image.
 @return Returns an initialized object.
 */
- (instancetype)initWithToolbarPrevButtonTitle:(NSString *)prevTitle
                               nextButtonTitle:(NSString *)nextTitle
                               doneButtonTitle:(NSString *)doneTitle
                                  withDelegate:(id)delegate;



/*!
 @description This method is used for initialize toolbar instance variables with delegate. It is used when need to show the images on control button.
 @return Returns an initialized object.
 */
- (instancetype)initWithToolbarPrevButtonImageName:(NSString *)prevImageName
                               nextButtonImageName:(NSString *)nextImageName
                               doneButtonTitle:(NSString *)doneTitle
                                  withDelegate:(id)delegate;


/*!
 @description This method is used for disable the previous control button.
 @param imageName Contains the image name of disabled button.
 */
- (void)disblePreviousButton:(BOOL)disable
               withImageName:(NSString *)imageName;


/*!
 @description This method is used for disable the next control button.
 @param imageName Contains the image name of disabled button.
 */
- (void)disbleNextButton:(BOOL)disable
           withImageName:(NSString *)imageName;


/**
 This property will use for access the UICustomToolbarDelegate methods
 */
@property (strong, nonatomic) id<UICustomToolbarDelegate>toolbarDelegate;

@end


