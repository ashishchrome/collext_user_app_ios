//
//  CustomToolBar.m
//  WhoHasTheKids
//
//  Created by Nitin Kumar on 13/05/15.
//  Copyright (c) 2015 Nitin Kumar. All rights reserved.
//

#import "CLCustomToolBar.h"
#import "AppDelegate.h"


@implementation CLCustomToolBar
{
    // Create instance for toolbar button titles.
    NSString        *_nextBtnTitle;
    NSString        *_prevBtnTitle;
    NSString        *_doneBtnTitle;
    NSString        *_nextButtonImageName;
    NSString        *_prevButtonImageName;
    UIBarButtonItem *_previousButton;
    UIBarButtonItem *_nextButton;
    UIButton        *_prevCustomButton;
    UIButton        *_nextCustomButton;
}


- (instancetype)initWithToolbarPrevButtonTitle:(NSString *)prevTitle
                               nextButtonTitle:(NSString *)nextTitle
                               doneButtonTitle:(NSString *)doneTitle
                                  withDelegate:(id)delegate
{
    self = [super init];
    if (self)
    {
        // Set initial variables values and delegates
        _prevBtnTitle       = prevTitle;
        _nextBtnTitle       = nextTitle;
        _doneBtnTitle       = doneTitle;
        _toolbarDelegate    = delegate;
    }
    
    // Returns customToolbar
    return (CLCustomToolBar *)[self getCustomizedToolBar];
}


- (instancetype)initWithToolbarPrevButtonImageName:(NSString *)prevImageName
                               nextButtonImageName:(NSString *)nextImageName
                                   doneButtonTitle:(NSString *)doneTitle
                                      withDelegate:(id)delegate
{
    self = [super init];
    if (self)
    {
        _prevButtonImageName    = prevImageName;
        _nextButtonImageName    = nextImageName;
        _prevBtnTitle           = @"";
        _nextBtnTitle           = @"";
        _doneBtnTitle           = doneTitle;
        _toolbarDelegate        = delegate;
    }
    
    // Returns customToolbar
    return (CLCustomToolBar *)[self getCustomizedToolBar];
}


/*!
 @description To get customized toolbar.
 @return Returns custom toolbar object with custom buttons.
 */
- (UIToolbar *)getCustomizedToolBar
{
    [self setBackgroundColor:[UIColor lightGrayColor]];
    [self sizeToFit];
    
    _prevCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 0, 25)];
    [_prevCustomButton setBackgroundImage:[UIImage imageNamed:_prevButtonImageName] forState:UIControlStateNormal];
    [_prevCustomButton addTarget:self action:@selector(onToolbarButtonSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    _nextCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 0, 25)];
    [_nextCustomButton setBackgroundImage:[UIImage imageNamed:_nextButtonImageName] forState:UIControlStateNormal];
    [_nextCustomButton addTarget:self action:@selector(onToolbarButtonSelected:) forControlEvents:UIControlEventTouchUpInside];

    [_prevCustomButton setTag:UIToolbarButtonTagPrevious];
    [_nextCustomButton setTag:UIToolbarButtonTagNext];
    
    _previousButton = [[UIBarButtonItem alloc] initWithCustomView:_prevCustomButton];
    
    _nextButton     = [[UIBarButtonItem alloc] initWithCustomView:_nextCustomButton];
    
    UIBarButtonItem *spaceButton    = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                   target:nil
                                                                                   action:nil];
    
    UIBarButtonItem *doneButton     = [[UIBarButtonItem alloc] initWithTitle:_doneBtnTitle
                                                                       style:UIBarButtonItemStyleDone
                                                                      target:nil
                                                                      action:@selector(onToolbarButtonSelected:)];
    
    UIBarButtonItem *tempButton     = [[UIBarButtonItem alloc] initWithTitle:nil
                                                                       style:UIBarButtonItemStyleDone
                                                                      target:nil
                                                                      action:nil];
    [doneButton setTintColor:DARK_ORANGE_COLOR];
    doneButton.tag      = UIToolbarButtonTagDone;
    
    _previousButton.width   = 20;
    _nextButton.width       = 20;
    tempButton.width        = 5;
    
    [self setItems:@[_previousButton, tempButton,_nextButton,spaceButton,doneButton]];
    
    return self;
}


- (void)disblePreviousButton:(BOOL)disable withImageName:(NSString *)imageName
{
    if (disable)
    {
        [_previousButton setEnabled:NO];
        [_prevCustomButton setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    else
    {
        [_previousButton setEnabled:YES];
        [_prevCustomButton setBackgroundImage:[UIImage imageNamed:_prevButtonImageName] forState:UIControlStateNormal];
    }
}


- (void)disbleNextButton:(BOOL)disable withImageName:(NSString *)imageName
{
    if (disable)
    {
        [_nextButton setEnabled:NO];
        [_nextCustomButton setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    else
    {
        [_nextButton setEnabled:YES];
        [_nextCustomButton setBackgroundImage:[UIImage imageNamed:_nextButtonImageName] forState:UIControlStateNormal];
    }
}


#pragma mark - ToolBar Button Selectors -

- (void)onToolbarButtonSelected:(UIButton *)sender
{
    if (sender.tag == UIToolbarButtonTagDone)
    {
        [APP_DELEGATE.window endEditing:YES];
    }
    else
    {
        // To inform that toolbar button is selected and send the sender's tag number
        [self.toolbarDelegate didToolbarButtonSelectedWithTag:sender.tag];
    }
}


@end
