//
//  PMSTextField.m
//  PriceMapSeller
//
//  Created by Nitin Kumar on 14/07/15.
//  Copyright (c) 2015 ChromeInfo Technologies. All rights reserved.
//

#import "CLTextField.h"

@implementation CLTextField

static CGFloat leftMargin = 5;

- (CGRect)textRectForBounds:(CGRect)bounds
{
    // Set left margin of displayed text in textfield.
    bounds.origin.x += leftMargin;
    return bounds;
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    // Set left margin of editing text.
    bounds.origin.x += leftMargin;
    return bounds;
}

- (void)drawRect:(CGRect)rect
{
    if ([self respondsToSelector:@selector(setAttributedPlaceholder:)])
    {
        if (self.placeholder.length > 0)
        {
            self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder
                                                                         attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
        }
    }
}


- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
//    if (action == @selector(paste:))
//    {
//        return NO;
//    }
    
    return NO;
}

@end
