//
//  CLBorderButton.m
//  Collext
//
//  Created by Nitin Kumar on 17/03/16.
//  Copyright © 2016 ChromeInfo Technologies. All rights reserved.
//

#import "CLBorderButton.h"

@implementation CLBorderButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.layer.borderWidth  = BORDER_WIDTH;
    self.layer.borderColor  = BORDER_COLOR;
    self.layer.cornerRadius = CORNER_RADIUS;
    self.clipsToBounds      = YES;
}

@end
