//
//  IPWebRequestHandler.m
//  InsightPortal
//
//  Created by Nitin Kumar on 23/10/15.
//  Copyright (c) 2015 ChromeInfo Technologies. All rights reserved.
//

#import "CLWebRequestHandler.h"
#import "AFNetworking.h"


#define NETWORK_TIME_OUT_INTERVAL 40

NSString *const kMimeType = @"image/png";

@implementation CLWebRequestHandler

+(instancetype)sharedInstance
{
    
    static CLWebRequestHandler *webConnection;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        // Get object
        webConnection = [[CLWebRequestHandler  alloc] init];
        
    });
    
    return webConnection;
    
}


- (void)sendPostRequestWithUrl:(NSString *)url
                   parameters:(NSDictionary *)parameters
         andCompletionHandler:(void (^)(NSDictionary *))successBlock
                 andOnFailure:(void (^)(bool))failed
{
    
    NSLog(@"URL: %@ and Params: %@", url, parameters);
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:url]];
    [manager.requestSerializer setTimeoutInterval:NETWORK_TIME_OUT_INTERVAL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:url
       parameters:parameters
         progress:nil
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
              
              NSLog(@"success!: %@", responseObject);
              
              successBlock(responseObject);
              
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              
              NSLog(@"error: %@", error);
              failed(false);
          }];

}


- (void)sendPOSTRequestWithUrl:(NSString *)url
                    parameters:(NSDictionary *)parameters
                         image:(id)image
          andCompletionHandler:(void (^)(NSDictionary *))successBlock
                  andOnFailure:(void (^)(bool))failed
{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager new];
    
    NSData *imageData = UIImagePNGRepresentation(image);
    
    //set time out interval
    [manager.requestSerializer setTimeoutInterval:NETWORK_TIME_OUT_INTERVAL];
    
    [manager setResponseSerializer:[AFJSONResponseSerializer new]]; //response will be returned in JSON format.
    
    [manager POST:url
       parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
           [formData appendPartWithFileData:imageData
                                       name:@""
                                   fileName:@"image_name"
                                   mimeType:kMimeType];
       } progress:^(NSProgress * _Nonnull uploadProgress) {
           
           NSLog(@"Upload progress");
           
       } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           
           successBlock(responseObject);
           
       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           
           failed(false);
           
       }];
}


- (void)monitorInternetConnection
{
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
     {
         switch (status)
         {
             case AFNetworkReachabilityStatusReachableViaWWAN:
             case AFNetworkReachabilityStatusReachableViaWiFi:
                 
                 // -- Reachable -- //
                 
                 self.isOnline = YES;
                 
                 break;
                 
             case AFNetworkReachabilityStatusNotReachable:
             default:
                 
                 // -- Not reachable -- //
                 self.isOnline = NO;
                 
                 break;
         }
     }];
    
}


@end
