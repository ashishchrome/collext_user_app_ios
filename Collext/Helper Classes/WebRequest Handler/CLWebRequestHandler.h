//
//  IPWebRequestHandler.h
//  InsightPortal
//
//  Created by Nitin Kumar on 23/10/15.
//  Copyright (c) 2015 ChromeInfo Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CLWebRequestHandler : NSObject


/*! Sends a async HTTP Post request to server and respond back using Blocks.
 @param URL: Server address, Parameters: Reuest body and Success & Failure blocks
 @return Void
 */
- (void) sendPostRequestWithUrl:(NSString*)url
                    parameters:(NSDictionary*)parameters
          andCompletionHandler:(void(^)(NSDictionary *response))successBlock
                  andOnFailure:(void(^)(bool isFailed))failed;



/*! Sends a async HTTP POST request to server and respond back using Blocks.
 @param URL: Server address, Parameters: Request body, Image: Image to post and Success & Failure blocks
 @return Void
 */
- (void) sendPOSTRequestWithUrl:(NSString*)url
                     parameters:(NSDictionary*)parameters
                          image:(UIImage *)image
           andCompletionHandler:(void(^)(NSDictionary *response))successBlock
                   andOnFailure:(void(^)(bool isFailed))failed;



/*! This method will monitor interner connection for this app. You can check isOnline property of App Delegate class for before making any network call.
 @param No Parameters
 @return Void
 */
- (void)monitorInternetConnection;


/*! To get shared instance.
 */
+(instancetype)sharedInstance;

@property (assign, nonatomic) BOOL isOnline;

@end
