//
//  main.m
//  Collext
//
//  Created by Nitin Kumar on 16/03/16.
//  Copyright © 2016 ChromeInfo Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
